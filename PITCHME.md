@snap[midpoint span-60]
#### Content-as-a-service
![uncached image](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://bitbucket.org/robert_barrow_MMT/ppg_decoupled/raw/HEAD/caas_api_first.puml)
@snapend

---
@snap[north]
#### Userful links
@snapend

@snap[midpoint span-50]
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

[View presentation](https://gitpitch.com/robert_barrow_MMT/ppg_decoupled/master?grs=bitbucket)

[PlantUML Guide](http://plantuml.com/guide)
@snapend